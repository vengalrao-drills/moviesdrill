// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime

const favouritesMovies = require("./data/2-movies");

function checkIt(word) {
  let genre = ["crime", "thriller", "adventure", "sci-fi ", "drama"];
  let final = "";
  genre.forEach((index) => {
    if (word.includes(index)) {
      final = index;
    }
  });
  return final ;
}

function q5(data) {
  let finalAns = {};
  for (let index in data) {
    let word = data[index].genre.join(" ").toLowerCase();
    let value = checkIt(word);

    if (!finalAns[value] || finalAns[value].length === 0) {
      finalAns[value] = [index];
    } else {
      finalAns[value].push(index);
    }
  }
  return finalAns;
}

console.log(q5(favouritesMovies));
