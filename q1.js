// Q1. Find all the movies with total earnings more than $500M.

const favouritesMovies = require("./data/2-movies");
function q1(favouritesMovies) {
  let blockBuster = {};
  for (index in favouritesMovies) {
    let movies = favouritesMovies[index];
    let earnings = movies.totalEarnings.replace(/[^0-9]/g, ""); // except 0-9 replace all the characters with space ''.
    if (Number(earnings) > 500) {
      blockBuster[index] = favouritesMovies[index];
    }
  }
  return blockBuster;
}

console.log(q1(favouritesMovies));
