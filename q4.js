// Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.

const favouritesMovies = require('./data/2-movies');

function q4(data){
    let movies = {}
    let allMovies = Object.entries(data) 
    let sortedMovies = allMovies.sort((a, b)=>{
        let aImdb = a[1].imdbRating
        let bImbd = b[1].imdbRating
         
        let aTotalEarning = a[1].totalEarnings.replace(/[^0-9]/g, ""); // except 0-9 replace all the characters with space ''.
        let bTotalEarning = b[1].totalEarnings.replace(/[^0-9]/g, "");
        if(aImdb !=bImbd ){
            return -(aImdb-bImbd)
        }else{
            return -(aTotalEarning - bTotalEarning)
        }
    })
    return sortedMovies
}

// q4(favouritesMovies)
console.log(q4(favouritesMovies))