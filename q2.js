// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

const favouritesMovies = require('./data/2-movies');

function q2(favouritesMovies){
    let blockBuster =  {}  
    for( index in favouritesMovies){ 
        let movies = favouritesMovies[index]
        let earnings = movies.totalEarnings.replace( /[^0-9]/g  ,'')  // except 0-9 replace all the characters with space ''.
        // console.log(earnings)
        if(Number(earnings)>500 & movies.oscarNominations >3 ){
            blockBuster[index] = favouritesMovies[index];
        }

    }
    return blockBuster

}

console.log(q2(favouritesMovies))