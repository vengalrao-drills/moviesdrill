// Q.3 Find all movies of the actor "Leonardo Dicaprio".

const favouritesMovies = require("./data/2-movies");

function q3(data) {
  let movie = {};
  for (let index in data) {
    let word = data[index]["actors"].join(",").toLowerCase();
    if (word.includes("leonardo dicaprio")) {
      movie[index] = data[index];
    }
  }
  return movie;
}

console.log(q3(favouritesMovies));
